package service;

import domain.Client;
import domain.Movie;
import domain.Rent;
import domain.validators.ValidatorException;
import repository.IPagingRepository;
import repository.IRepository;
import repository.Pageable;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static java.util.stream.Collectors.counting;
import static java.util.stream.Collectors.groupingBy;

/**
 * @author Patricia
 */

public class RentService {
    private IPagingRepository<Integer, Rent> rentRepository;
    private IPagingRepository<Integer, Movie> movieRepository;
    private IPagingRepository<Integer, Client> clientRepository;

    private int page = 0;
    private int size = 1;

    public RentService(IPagingRepository<Integer, Rent> repository, IPagingRepository<Integer, Movie> movieRepository, IPagingRepository<Integer, Client> clientRepository){
        this.rentRepository = repository;
        this.movieRepository = movieRepository;
        this.clientRepository = clientRepository;
    }

    public void addRent(Rent rent) throws ValidatorException{
        rentRepository.save(rent);
    }

    public Set<Rent> getAllRents(){
        Iterable<Rent>rents = rentRepository.findAll();
        return StreamSupport.stream(rents.spliterator(), false).collect(Collectors.toSet());
    }

    public void removeRent(Integer id) throws IllegalArgumentException{
        rentRepository.delete(id);
    }

    public void updateRent(Rent newRent) throws IllegalArgumentException{
        rentRepository.update(newRent);
    }

    public Movie findMovieNameByID(Integer id){
        Iterable<Movie> ml = movieRepository.findAll();
        List<Movie> movieList = StreamSupport.stream(ml.spliterator(), false).collect(Collectors.toList());
        List<Movie> finalList = movieList.stream().filter(e-> e.getId().equals(id)).collect(Collectors.toList());
        return finalList.get(0);
    }

    public String rentStatistics(){
        Set<Rent> rents = getAllRents();
        List<Integer> l = new ArrayList<Integer>();
        rents.forEach(e-> l.add(e.getMovieID()));
        //Collections.sort(l);
        Map<Integer, Long> collect =
                l.stream().collect(groupingBy(Function.identity(), counting()));
        LinkedHashMap<Integer, Long> countByWordSorted = collect.entrySet()
                .stream()
                .sorted(Map.Entry.comparingByValue(Comparator.reverseOrder()))
                .collect(Collectors.toMap(
                        Map.Entry::getKey,
                        Map.Entry::getValue,
                        (v1, v2) -> {
                            throw new IllegalStateException();
                        },
                        LinkedHashMap::new
                ));
        Set<Integer> set = countByWordSorted.keySet();
        List<Movie> namel = new ArrayList<Movie>();
        set.forEach(e->namel.add(findMovieNameByID(e)));
        return namel.toString();
    }

    public void setPageSize(int size){
        this.size = size;
    }

    public Set<Rent> getNextRents(){
        Set<Rent> rt = rentRepository.findAll(new Pageable(page, size)).getContent().collect(Collectors.toSet());
        page += 1;

        return rt;
    }
}

