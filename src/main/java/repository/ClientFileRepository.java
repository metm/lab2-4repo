package repository;

import domain.Client;
import domain.Movie;
import domain.validators.IValidator;
import domain.validators.ValidatorException;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

/**
 * @author Diana
 */

public class ClientFileRepository extends InMemoryRepository<Integer, Client> {
    private String fileName;

    public ClientFileRepository(IValidator<Client> validator, String fileName) {
        super(validator);
        this.fileName=fileName;

        loadData();
    }

    private void loadData() {
        Path path = Paths.get(fileName);

        try {
            Files.lines(path).forEach(line -> {
                List<String> items = Arrays.asList(line.split(","));

                Integer id = Integer.valueOf(items.get(0));
                String clientFirstName = items.get(1);
                String clientLastName = items.get((2));

                Client client=new Client(clientFirstName,clientLastName);
                client.setId(id);

                try {
                    super.add(client);
                } catch (ValidatorException e) {
                    e.printStackTrace();
                }
            });
        }  catch (IOException e) {
            e.printStackTrace();
        }
    }


    @Override
    public Optional<Client> add(Client entity) throws ValidatorException {
        Optional<Client> optional = super.add(entity);
        if (optional.isPresent()) {
            return optional;
        }
        saveToFile(entity);
        return Optional.empty();
    }

    @Override
    public Optional<Client> remove(Integer integer) throws IllegalArgumentException {
        return super.remove(integer);
    }

    @Override
    public Optional<Client> update(Client newMovie) throws ValidatorException {
        return super.update(newMovie);
    }

    private void saveToFile(Client entity) {
        Path path = Paths.get(fileName);

        try (BufferedWriter bufferedWriter = Files.newBufferedWriter(path, StandardOpenOption.APPEND)) {
            bufferedWriter.write(
                    entity.getId() + "," + entity.getfName() + "," + entity.getlName());
            bufferedWriter.newLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
