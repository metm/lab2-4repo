package repository;

import domain.Movie;
import domain.validators.IValidator;
import domain.validators.ValidatorException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class MovieXMLRepository extends InMemoryRepository<Integer, Movie> {

    public MovieXMLRepository(IValidator<Movie> validator) {
        super(validator);
        try {
            loadData();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void loadData() throws Exception {
        //List<Movie> listOfMovies = new ArrayList<>();
        DocumentBuilderFactory documentBuilderFactory =
                DocumentBuilderFactory.newInstance();
        DocumentBuilder documentBuilder =
                documentBuilderFactory.newDocumentBuilder();
        Document document = documentBuilder.parse("data/movies.xml");
        Element root = document.getDocumentElement();

        NodeList nodes = root.getChildNodes();
        int len = nodes.getLength();
        for (int i = 0; i < len; i++) {
            Node movieNode = nodes.item(i);
            if (movieNode instanceof Element) {
                Movie m = createMovie((Element) movieNode);
                //listOfMovies.add(m);
                super.add(m);
            }
        }
        //return listOfMovies;
    }

    private static void saveMovie(Movie movie) throws Exception {
        Document document = DocumentBuilderFactory
                .newInstance()
                .newDocumentBuilder()
                .parse("data/movies.xml");
        Element root = document.getDocumentElement();

        Element movieElement = document.createElement("movie");
        movieElement.setAttribute("id", String.valueOf(movie.getId()));
        root.appendChild(movieElement);

        appendChildWithText(document, movieElement, "name", movie.getMovieName());
        appendChildWithText(document, movieElement, "description", movie.getMovieDescription());
        appendChildWithText(document, movieElement, "genre", movie.getMovieGenre());

        Transformer transformer =
                TransformerFactory.newInstance().newTransformer();
        transformer.transform(new DOMSource(root),
                new StreamResult(new FileOutputStream(
                        "./data/movies.xml")));
    }

    private static void appendChildWithText(Document document,
                                            Node parent, String tagName, String textContent) {
        Element element = document.createElement(tagName);
        element.setTextContent(textContent);
        parent.appendChild(element);
    }

    private static Movie createMovie(Element movieNode) {
        Integer id = Integer.valueOf(movieNode.getAttribute("id"));
        Movie m = new Movie("", "", "");
        m.setId(id);

        m.setMovieName(getTextFromTagName(movieNode, "name"));
        m.setMovieDescription(getTextFromTagName(movieNode, "description"));
        m.setMovieGenre(getTextFromTagName(movieNode, "genre"));
        return m;
    }

    private static String getTextFromTagName(Element element, String tagName) {
        NodeList elements = element.getElementsByTagName(tagName);
        Node node = elements.item(0);
        return node.getTextContent();
    }

    @Override
    public Optional<Movie> add(Movie entity) throws ValidatorException {
        Optional<Movie> optional = super.add(entity);
        if (optional.isPresent()) {
            return optional;
        }
        try {
            saveMovie(entity);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return Optional.empty();
    }

    @Override
    public Optional<Movie> remove(Integer integer) throws IllegalArgumentException {
        return super.remove(integer);
    }

    @Override
    public Optional<Movie> update(Movie newMovie) throws ValidatorException {
        return super.update(newMovie);
    }
}
