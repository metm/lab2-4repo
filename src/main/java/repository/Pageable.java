package repository;

public class Pageable implements IPageable {
    private int pageNumber;
    private int pageSize;

    public Pageable(int nr, int size){
        this.pageNumber = nr;
        this.pageSize = size;
    }
    @Override
    public int getPageNumber() {
        return this.pageNumber;
    }

    @Override
    public int getPageSize() {
        return this.pageSize;
    }
}
