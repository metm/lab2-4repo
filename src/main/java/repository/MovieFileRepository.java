package repository;

import domain.Movie;
import domain.validators.IValidator;
import domain.validators.ValidatorException;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

/**
 * @author Diana
 */

public class MovieFileRepository extends InMemoryRepository<Integer, Movie>{
    private String fileName;

    public MovieFileRepository(IValidator<Movie> validator, String fileName) {
        super(validator);
        this.fileName=fileName;

        loadData();
    }

    private void loadData() {
        Path path = Paths.get(fileName);

        try {
            Files.lines(path).forEach(line -> {
                List<String> items = Arrays.asList(line.split(","));

                Integer id = Integer.valueOf(items.get(0));
                String movieName = items.get(1);
                String movieDescription = items.get((2));
                String movieGenre = items.get(3);

                Movie movie = new Movie(movieName,movieDescription,movieGenre);
                movie.setId(id);

                try {
                    super.add(movie);
                } catch (ValidatorException e) {
                    e.printStackTrace();
                }
            });
        }  catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Optional<Movie> add(Movie entity) throws ValidatorException {
        Optional<Movie> optional = super.add(entity);
        if (optional.isPresent()) {
            return optional;
        }
        saveToFile(entity);
        return Optional.empty();
    }

    @Override
    public Optional<Movie> remove(Integer integer) throws IllegalArgumentException {
        return super.remove(integer);
    }

    @Override
    public Optional<Movie> update(Movie newMovie) throws ValidatorException {
        return super.update(newMovie);
    }


    private void saveToFile(Movie entity) {
        Path path = Paths.get(fileName);

        try (BufferedWriter bufferedWriter = Files.newBufferedWriter(path, StandardOpenOption.APPEND)) {
            bufferedWriter.write(
                    entity.getId() + "," + entity.getMovieName() + "," + entity.getMovieDescription() + "," + entity.getMovieGenre());
            bufferedWriter.newLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
