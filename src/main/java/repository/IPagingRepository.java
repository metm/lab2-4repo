package repository;

import domain.BaseEntity;

import java.io.Serializable;

public interface IPagingRepository<ID extends Serializable,
        T extends BaseEntity<ID>>
        extends CrudRepository<ID, T> {

    IPage<T> findAll(IPageable pageable);

    //TODO: any other methods are allowed...

}
