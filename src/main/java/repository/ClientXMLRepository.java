package repository;

import domain.Client;
import domain.Movie;
import domain.validators.IValidator;
import domain.validators.ValidatorException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Optional;

public class ClientXMLRepository extends InMemoryRepository<Integer, Client> {
    public ClientXMLRepository(IValidator<Client> validator) {
        super(validator);
        try{
            loadData();
        } catch (Exception e){
            e.printStackTrace();
        }
    }

    private void loadData() throws Exception {
        //List<Movie> listOfMovies = new ArrayList<>();
        DocumentBuilderFactory documentBuilderFactory =
                DocumentBuilderFactory.newInstance();
        DocumentBuilder documentBuilder =
                documentBuilderFactory.newDocumentBuilder();
        Document document = documentBuilder.parse("data/clients.xml");
        Element root = document.getDocumentElement();

        NodeList nodes = root.getChildNodes();
        int len = nodes.getLength();
        for (int i = 0; i < len; i++) {
            Node clientNode = nodes.item(i);
            if (clientNode instanceof Element) {
                Client c = createClient((Element) clientNode);
                //listOfMovies.add(m);
                super.add(c);
            }
        }
        //return listOfMovies;
    }

    private static void saveClient(Client client) throws Exception {
        Document document = DocumentBuilderFactory
                .newInstance()
                .newDocumentBuilder()
                .parse("data/clients.xml");
        Element root = document.getDocumentElement();

        Element clientElement = document.createElement("client");
        clientElement.setAttribute("id", String.valueOf(client.getId()));
        root.appendChild(clientElement);

        appendChildWithText(document, clientElement, "firstName", client.getfName());
        appendChildWithText(document, clientElement, "lastName", client.getlName());

        Transformer transformer =
                TransformerFactory.newInstance().newTransformer();
        transformer.transform(new DOMSource(root),
                new StreamResult(new FileOutputStream(
                        "./data/clients.xml")));
    }

    private static void appendChildWithText(Document document,
                                            Node parent, String tagName, String textContent) {
        Element element = document.createElement(tagName);
        element.setTextContent(textContent);
        parent.appendChild(element);
    }

    private static Client createClient (Element clientNode) {
        Integer id = Integer.valueOf(clientNode.getAttribute("id"));
        Client c = new Client("", "");
        c.setId(id);

        c.setfName(getTextFromTagName(clientNode, "firstName"));
        c.setlName(getTextFromTagName(clientNode, "lastName"));
        //c.setMovieGenre(getTextFromTagName(movieNode, "genre"));
        return c;
    }

    private static String getTextFromTagName(Element element, String tagName) {
        NodeList elements = element.getElementsByTagName(tagName);
        Node node = elements.item(0);
        return node.getTextContent();
    }

    @Override
    public Optional<Client> add(Client entity) throws ValidatorException {
        Optional<Client> optional = super.add(entity);
        if (optional.isPresent()) {
            return optional;
        }
        try {
            saveClient(entity);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return Optional.empty();
    }

    @Override
    public Optional<Client> remove(Integer integer) throws IllegalArgumentException {
        return super.remove(integer);
    }

    @Override
    public Optional<Client> update(Client newClient) throws ValidatorException {
        return super.update(newClient);
    }
}
