package repository;

import domain.Rent;
import domain.newValidators.Validator;
import domain.validators.IValidator;
import domain.validators.ValidatorException;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;


/**
 * @author Patricia
 */

public class RentFileRepository extends InMemoryRepository<Integer, Rent>{
    private String fileName;

    public RentFileRepository(IValidator<Rent> validator, String filename) {
        super(validator);
        this.fileName = filename;

        loadData();
    }

    private void loadData(){
        Path path = Paths.get(fileName);

        try{
            Files.lines(path).forEach(line->{
                List<String> items = Arrays.asList(line.split(","));
                Integer id = Integer.valueOf(items.get(0));
                Integer movieID = Integer.valueOf(items.get(1));
                Integer clientID = Integer.valueOf(items.get(2));
                String startDate = items.get(3);
                String dueDate = items.get(4);

//                Rent rent = new Rent(movieID, clientID, startDate, dueDate);
//                rent.setId(id);

                try{
                    //super.add(rent);
                } catch (ValidatorException e){
                    e.printStackTrace();
                }
            });
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void saveToFile(Rent entity){
        Path path = Paths.get(fileName);

        try (BufferedWriter bufferedWriter = Files.newBufferedWriter(path, StandardOpenOption.APPEND)) {
            bufferedWriter.write(
                    entity.getId() + "," + entity.getMovieID() + "," + entity.getClientID() + "," + entity.getStartDate() + "," + entity.getDueDate());
            bufferedWriter.newLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Optional<Rent> add(Rent entity) throws ValidatorException {
        Optional<Rent> optional = super.add(entity);
        if(optional.isPresent()){
            return optional;
        }
        saveToFile(entity);
        return Optional.empty();
    }

    @Override
    public Optional<Rent> remove(Integer integer) throws IllegalArgumentException {
        return super.remove(integer);
    }

    @Override
    public Optional<Rent> update(Rent newMovie) throws ValidatorException {
        return super.update(newMovie);
    }
}
