package repository;

import domain.Movie;
import domain.Rent;
import domain.validators.IValidator;
import domain.validators.ValidatorException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.FileOutputStream;
import java.util.Optional;

public class RentXMLRepository extends InMemoryRepository<Integer, Rent> {
    public RentXMLRepository(IValidator<Rent> validator) {
        super(validator);
        try{
            loadData();
        } catch (Exception e){
            e.printStackTrace();
        }
    }

    private void loadData() throws Exception {
        //List<Movie> listOfMovies = new ArrayList<>();
        DocumentBuilderFactory documentBuilderFactory =
                DocumentBuilderFactory.newInstance();
        DocumentBuilder documentBuilder =
                documentBuilderFactory.newDocumentBuilder();
        Document document = documentBuilder.parse("data/rents.xml");
        Element root = document.getDocumentElement();

        NodeList nodes = root.getChildNodes();
        int len = nodes.getLength();
        for (int i = 0; i < len; i++) {
            Node rentNode = nodes.item(i);
            if (rentNode instanceof Element) {
                Rent r = createRent((Element) rentNode);
                //listOfMovies.add(m);
                super.add(r);
            }
        }
        //return listOfMovies;
    }

    private static void saveRent(Rent rent) throws Exception {
        Document document = DocumentBuilderFactory
                .newInstance()
                .newDocumentBuilder()
                .parse("data/rents.xml");
        Element root = document.getDocumentElement();

        Element rentElement = document.createElement("rent");
        rentElement.setAttribute("id", String.valueOf(rent.getId()));
        root.appendChild(rentElement);

        appendChildWithText(document, rentElement, "movieID",String.valueOf(rent.getMovieID()));
        appendChildWithText(document, rentElement, "clientID", String.valueOf(rent.getClientID()));
        //appendChildWithText(document, rentElement, "startDate", rent.getStartDate());
        //appendChildWithText(document, rentElement, "dueDate", rent.getDueDate());

        Transformer transformer =
                TransformerFactory.newInstance().newTransformer();
        transformer.transform(new DOMSource(root),
                new StreamResult(new FileOutputStream(
                        "./data/rents.xml")));
    }

    private static void appendChildWithText(Document document,
                                            Node parent, String tagName, String textContent) {
        Element element = document.createElement(tagName);
        element.setTextContent(textContent);
        parent.appendChild(element);
    }

    private static Rent createRent(Element rentNode) {
        Integer id = Integer.valueOf(rentNode.getAttribute("id"));
        //Rent r = new Rent(0, 0, "", "");
        //r.setId(id);

        //r.setMovieID(Integer.valueOf(getTextFromTagName(rentNode, "movieID")));
        //r.setClientID(Integer.valueOf(getTextFromTagName(rentNode, "clientID")));
       // r.setStartDate(getTextFromTagName(rentNode, "startDate"));
        //r.setDueDate(getTextFromTagName(rentNode, "dueDate"));
        //return r;
        return null;
    }

    private static String getTextFromTagName(Element element, String tagName) {
        NodeList elements = element.getElementsByTagName(tagName);
        Node node = elements.item(0);
        return node.getTextContent();
    }

    @Override
    public Optional<Rent> add(Rent entity) throws ValidatorException {
        Optional<Rent> optional = super.add(entity);
        if (optional.isPresent()) {
            return optional;
        }
        try {
            saveRent(entity);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return Optional.empty();
    }
}
