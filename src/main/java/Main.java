import domain.Client;
import domain.Movie;
import domain.Rent;
import domain.newValidators.LambdaMovieValidator;
import domain.validators.ClientValidator;
import domain.validators.IValidator;
import domain.validators.MovieValidator;
import domain.validators.RentValidator;
import repository.*;
import service.ClientService;
import service.MovieService;
import service.RentService;
import ui.Console;
import domain.newValidators.*;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Set;

public class Main {
    public static void main(String[] args) {
        //Movie movie1=new Movie("The Notebook","a love story","romance");
        //movie1.setId(1);
        //Movie movie2=new Movie(1,"","a love story","romance");
        //System.out.println(movie1.toString());
        IValidator<Movie> movieValidator=new MovieValidator();
        //Validator<Movie> movieValidator = new LambdaMovieValidator();
        IValidator<Client> clientValidator=new ClientValidator();
        IValidator<Rent> rentValidator = new RentValidator();
        //IRepository<Integer, Movie> movieRepository = new InMemoryRepository<>(movieValidator);
        //IRepository<Integer, Movie> movieRepository = new MovieFileRepository(movieValidator,"./data/movies");
        //IRepository<Integer, Movie> movieRepository=new MovieXMLRepository(movieValidator);
        //IRepository<Integer, Client> clientRepository=new InMemoryRepository<>(clientValidator);
        //IRepository<Integer, Client> clientRepository=new ClientFileRepository(clientValidator,"./data/clients");
        //IRepository<Integer, Rent> rentRepository = new RentFileRepository(rentValidator, "./data/rents");
        //IRepository<Integer, Client> clientRepository = new ClientXMLRepository(clientValidator);
        IPagingRepository<Integer, Client> paginatedClientRepo = new ClientDBRepo(clientValidator);
        IPagingRepository<Integer, Movie> paginatedMovieRepo = new MovieDBRepo(movieValidator);
        IPagingRepository<Integer, Rent> paginatedRentRepo = new RentDBRepo(rentValidator);
        //IRepository<Integer, Rent> rentRepository = new RentXMLRepository(rentValidator);
        MovieService movieService = new MovieService(paginatedMovieRepo);
        //MovieService movieService = new MovieService(movieRepository);
        //ClientService clientService=new ClientService(clientRepository);
        ClientService clientService=new ClientService(paginatedClientRepo);
        RentService rentService = new RentService(paginatedRentRepo, paginatedMovieRepo, paginatedClientRepo);
        Console ui = new Console(movieService,clientService, rentService);
        try {
            ui.runConsole();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
