package domain.newValidators;

import domain.Movie;

import static domain.newValidators.StringValidationHelper.between;
import static domain.newValidators.StringValidationHelper.notNull;

public class LambdaMovieValidator implements Validator<Movie> {
    public void validate(Movie movie) {
        notNull.and(between(2, 50)).test(movie.getMovieName()).throwIfInvalid("name");
        notNull.and(between(4, 100)).test(movie.getMovieDescription()).throwIfInvalid("description");
        notNull.and(between(2, 100)).test(movie.getMovieGenre()).throwIfInvalid("genre");
    }
}
