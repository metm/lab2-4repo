package domain.newValidators;

import domain.Movie;

public interface Validator<T> {
    void validate(T t);
}
