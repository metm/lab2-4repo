package domain.validators;

/**
 * @author Diana
 */

public interface IValidator<T> {
    void validate(T entity) throws ValidatorException;
}
