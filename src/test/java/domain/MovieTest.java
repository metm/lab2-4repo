package domain;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class MovieTest {
    private String movieName = "Titanic";
    private String movieDescription = "Love story";
    private String movieGenre = "romance";
    private Movie m1;

    @Before
    public void setUp() throws Exception {
        m1 = new Movie(movieName, movieDescription, movieGenre);
    }

    @Test
    public void getMovieName() {
        assertEquals("TestGetMovieName", "Titanic", m1.getMovieName());
    }

    @Test
    public void getMovieDescription() {
        assertEquals("TestGetMovieDescription", "Love story", m1.getMovieDescription());
    }

    @Test
    public void getMovieGenre() {
        assertEquals("TestGetMovieGenre", "romance", m1.getMovieGenre());
    }

    @Test
    public void setMovieName() {
        m1.setMovieName("The Notebook");
        assertEquals("TestGetMovieName", "The Notebook", m1.getMovieName());
    }

    @Test
    public void setMovieDescription() {
        m1.setMovieDescription("love story");
        assertEquals("TestGetMovieDescription", "love story", m1.getMovieDescription());
    }

    @Test
    public void setMovieGenre() {
        m1.setMovieGenre("Romance");
        assertEquals("TestGetMovieGenre", "Romance", m1.getMovieGenre());
    }

    @Test
    public void testtoString() {
        assertEquals("testToSTring", "null. Titanic - Description: Love story, Genre: romance", m1.toString());
    }

    @Test
    public void equals() {
        assertTrue("TestEqual", m1.equals(m1));
    }
}