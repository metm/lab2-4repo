package repository;

import domain.Movie;
import domain.validators.IValidator;
import domain.validators.MovieValidator;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class InMemoryRepositoryTest {
    private IRepository<Integer, Movie> repo;
    private Movie m1;

    @Before
    public void setUp() throws Exception {
        IValidator<Movie> movieValidator = new MovieValidator();
        repo = new InMemoryRepository<>(movieValidator);
        m1 = new Movie("Titanic", "Love", "Romance");
        m1.setId(1);
    }

    @Test
    public void add() {
        repo.add(m1);
        assertEquals("[1. Titanic - Description: Love, Genre: Romance]", repo.getAll().toString());
    }

    @Test
    public void getAll() {
        assertEquals("[]", repo.getAll().toString());
        repo.add(m1);
        assertEquals("[1. Titanic - Description: Love, Genre: Romance]", repo.getAll().toString());
    }

    @Test
    public void remove() {
        repo.add(m1);
        repo.remove(1);
        assertEquals("[]",repo.getAll().toString());
    }

    @Test
    public void update() {
        repo.add(m1);
        Movie m2=new Movie("Titanic","Love story","Romance");
        m2.setId(m1.getId());
        repo.update(m2);
        assertEquals("[1. Titanic - Description: Love story, Genre: Romance]",repo.getAll().toString());
    }
}