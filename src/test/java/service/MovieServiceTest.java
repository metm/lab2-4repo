package service;

import domain.Movie;
import domain.validators.IValidator;
import domain.validators.MovieValidator;
import org.junit.Before;
import org.junit.Test;
import repository.IRepository;
import repository.InMemoryRepository;

import static org.junit.Assert.*;

public class MovieServiceTest {
    private IValidator<Movie> movieValidator;
    private IRepository<Integer, Movie> movieRepository;
    private MovieService movieService;
    private Movie m1;

    @Before
    public void setUp() throws Exception {
         movieValidator = new MovieValidator();
         movieRepository = new InMemoryRepository<>(movieValidator);
         movieService = new MovieService(movieRepository);
         m1 = new Movie("Titanic", "Love", "Romance");
    }

    @Test
    public void addMovie() {
        movieService.addMovie(m1);
        m1.setId(1);
        assertEquals(1, movieService.getAllMovies().size());
        Movie m2 = new Movie("Up", "for children", "animation");
        m2.setId(2);
        movieService.addMovie(m2);
        assertEquals(2, movieService.getAllMovies().size());
    }

    @Test
    public void getAllMovies() {
        movieService.addMovie(m1);
        m1.setId(1);
        assertEquals("[1. Titanic - Description: Love, Genre: Romance]", movieService.getAllMovies().toString());
    }

    @Test
    public void removeMovie() {
        m1.setId(1);
        movieService.addMovie(m1);
        movieService.removeMovie(1);
        assertEquals(0,movieService.getAllMovies().size());
    }

    @Test
    public void updateMovie() {
        m1.setId(1);
        movieService.addMovie(m1);
        Movie m2=new Movie("Titanic","A love story","Drama");
        m2.setId(m1.getId());
        movieService.updateMovie(m2);
        assertEquals("[1. Titanic - Description: A love story, Genre: Drama]", movieService.getAllMovies().toString());
    }
}